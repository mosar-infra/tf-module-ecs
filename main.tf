# ecs/main.tf

data "template_file" "container_specs" {
  for_each              = var.container_spec_definitions
  template              = each.value.spec_file
  vars                  = {
    image_tag           = var.image_tag
    log_group_name      = var.log_group_name
    database_flashcards = var.database_flashcards
    db_user             = var.db_user
    db_host             = var.db_host
    truststore_pass     = var.truststore_pass
    environment         = var.environment
    spring_profile      = var.spring_profile
  }
}

resource "aws_ecs_task_definition" "task_definition" {
  for_each                 = var.container_spec_definitions
  family                   = each.value.family
  network_mode             = "awsvpc"
  execution_role_arn       = var.ecs_task_execution_role.arn
  task_role_arn            = var.ecs_task_role.arn
  cpu                      = 256
  memory                   = 2048
  requires_compatibilities = ["FARGATE"]
  container_definitions    = data.template_file.container_specs[each.key].rendered
  tags                     = {
    Name = "mosar ecs task definition"
    Environment = var.environment
  }
  depends_on               = [aws_ecs_cluster.cluster]
}

resource "aws_ecs_service" "service" {
  for_each        = var.container_spec_definitions
  name            = "${each.value.name}-${var.environment}-ecs-service"
  cluster         = aws_ecs_cluster.cluster.id
  task_definition = aws_ecs_task_definition.task_definition[each.key].arn
  desired_count   = each.value.desired_count
  launch_type     = "FARGATE"

  network_configuration {
    security_groups  = each.value.security_groups
    subnets          = each.value.subnet_ids
    assign_public_ip = false
  }

  dynamic load_balancer {
    for_each           = each.value.use_load_balancer ? [1] : []
    content {
      target_group_arn = var.lb_target_group_arn
      container_name   = var.lb_container_name
      container_port   = var.lb_container_port
    }
  }

  tags = {
    Name = each.value.name
    Environment = var.environment
  }

  service_registries {
    registry_arn = aws_service_discovery_service.discovery_service[each.key].arn
  }
}

resource "aws_kms_key" "log_key" {
  description             = "key to encrypt data between the client and the container"
  deletion_window_in_days = 7
}

resource "aws_ecs_cluster" "cluster" {
  name = var.cluster_name

  configuration {
    execute_command_configuration {
      kms_key_id = aws_kms_key.log_key.arn
      logging    = "OVERRIDE"

      log_configuration {
        cloud_watch_encryption_enabled = true
        cloud_watch_log_group_name = var.log_group_name
      }
    }
  }
}

resource "aws_service_discovery_private_dns_namespace" "dns_namespace" {
  name        = "mosar.local"
  description = "mosar namespace"
  vpc         = var.vpc_id
}

resource "aws_service_discovery_service" "discovery_service" {
  for_each = var.container_spec_definitions
  name = "${each.key}"

  dns_config {
    namespace_id = aws_service_discovery_private_dns_namespace.dns_namespace.id

    dns_records {
      ttl  = 30
      type = "A"
    }

    routing_policy = "MULTIVALUE"
  }

  health_check_custom_config {
    failure_threshold = 5
  }
}

