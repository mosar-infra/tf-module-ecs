# ecs/variables

variable "cluster_name" {}
variable "container_spec_definitions" {}
variable "database_flashcards" {}
variable "db_user" {}
variable "db_host" {}
variable "truststore_pass" {}
variable "spring_profile" {}
variable "ecs_task_execution_role" {}
variable "ecs_task_role" {}
variable "environment" {}
variable "image_tag" {}
variable "log_group_name" {}
variable "vpc_id" {}
variable "lb_container_name" {}
variable "lb_container_port" {}
variable "lb_target_group_arn" {}

